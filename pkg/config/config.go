package config

import (
	"VendingTgBot/models"
	"fmt"
	"github.com/spf13/viper"
	"gopkg.in/validator.v2"
	"log/slog"
)

const (
	ModeDebug = "debug"
)

func LoadConfig(configFilePath string) (*models.Config, error) {
	config, err := readConfig(configFilePath)
	if err != nil {
		return nil, err
	}

	slog.Info("validating configuration")
	if err = validator.Validate(config); err != nil {
		return nil, err
	}
	slog.Info("configuration is valid")

	return config, nil
}

func readConfig(path string) (*models.Config, error) {
	slog.Info("prepare configuration file")
	viper.SetConfigFile(path)
	viper.AutomaticEnv()

	slog.Info("read from file")
	err := viper.ReadInConfig()
	if err != nil {
		return nil, fmt.Errorf("error reading config file: %v", err)
	}

	slog.Info("prepare config models")
	var config models.Config
	err = viper.Unmarshal(&config)
	if err != nil {
		return nil, fmt.Errorf("unable to decode into struct: %v", err)
	}

	slog.Info("config models is ready")
	return &config, nil
}
