package repository

import (
	"VendingTgBot/models"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func NewConnection(config *models.Config) (*models.Connection, error) {
	var connections models.Connection

	bot, err := tgbotapi.NewBotAPI(config.Telegram.ApiKey)
	if err != nil {
		return nil, err
	}
	connections.TelegramBot = bot

	return &connections, nil
}
