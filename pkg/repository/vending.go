package repository

type ExampleRepository struct {
	conn *interface{}
}

func NewExampleRepository(conn *interface{}) *ExampleRepository {
	return &ExampleRepository{conn: conn}
}

// @TODO here write your methods with returning
// @TODO - error for simple method
// @TODO - (*models.OutputExample, error) for complex method

func (r ExampleRepository) ExampleMethod() error {
	// @TODO write your logic here
	// @TODO use r.conn to access connections

	return nil
}
