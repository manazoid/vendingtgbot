package repository

import "VendingTgBot/models"

type Example interface {
	ExampleMethod() error
	// @TODO here write list of your repository methods
}

type Repository struct {
	Example
	// @TODO here write your interface names. each interface for group of tables
}

func NewRepository(conns *models.Connection) *Repository {
	return &Repository{
		Example: NewExampleRepository(conns.ExampleConnection),
		// @TODO every new interface must be initiated with New<TypeOfStorage>Repository method.
		// @TODO create separate file for it
	}
}
