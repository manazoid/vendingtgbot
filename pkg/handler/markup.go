package handler

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

func (h Handler) SendNewTxtAndMarkUp(userTgID int64, txt string, markup tgbotapi.InlineKeyboardMarkup) (tgbotapi.Message, error) {
	msg := tgbotapi.NewMessage(userTgID, txt)
	msg.ReplyMarkup = markup
	return h.bot.Send(msg)
}

func (h Handler) SendEditTxtAndMarkUp(userTgID int64, messageID int, txt string, markup tgbotapi.InlineKeyboardMarkup) (tgbotapi.Message, error) {
	msg := tgbotapi.NewEditMessageTextAndMarkup(userTgID, messageID, txt, markup)
	return h.bot.Send(msg)
}

func (h Handler) ChooseMarkUp(oldMarkUp *tgbotapi.InlineKeyboardMarkup, newMarkUp *tgbotapi.InlineKeyboardMarkup) tgbotapi.InlineKeyboardMarkup {
	if oldMarkUp == nil {
		output := tgbotapi.NewInlineKeyboardMarkup()
		oldMarkUp = &output
	}
	if newMarkUp == nil {
		return *oldMarkUp
	}
	output := *newMarkUp
	*oldMarkUp = output
	return output
}
