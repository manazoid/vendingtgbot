package handler

import (
	"VendingTgBot/models"
	"VendingTgBot/pkg/logging"
	"VendingTgBot/pkg/service"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type Handler struct {
	services *service.Service
	logger   *logging.Logger
	bot      *tgbotapi.BotAPI
	UserMemo map[int64]*models.User
}

func NewHandler(services *service.Service, logger *logging.Logger, bot *tgbotapi.BotAPI) *Handler {
	return &Handler{
		services: services,
		logger:   logger,
		bot:      bot,
		UserMemo: make(map[int64]*models.User),
	}
}

func (h Handler) Run(isDebug bool) error {
	var lastMarkUp *tgbotapi.InlineKeyboardMarkup
	h.bot.Debug = isDebug

	updateConfig := tgbotapi.NewUpdate(0)
	updateConfig.Timeout = 60
	updates := h.bot.GetUpdatesChan(updateConfig)

	for update := range updates {
		var reply string

		from := update.SentFrom()
		if from == nil {
			h.logger.ErrLogger.Printf("telegram do not provide information about sender: %v", update)
			continue
		}

		id := from.ID
		user, ok := h.UserMemo[id]

		if !ok {
			// Init user
			h.UserMemo[id] = &models.User{
				Language: from.LanguageCode, // Locale
			}
			user = h.UserMemo[id]

			action, ok := h.services.Actions[defaultOption]
			if !ok {
				h.logger.ErrLogger.Printf("invalid action: %v", defaultOption)
				continue
			}

			markUp, err := h.services.Vending.InlineMarkUp(action)
			if err != nil {
				h.logger.ErrLogger.Println(err)
				continue
			}

			lastMarkUp = markUp
			reply = action.Reply
			if _, err := h.SendNewTxtAndMarkUp(id, reply, *markUp); err != nil {
				h.logger.ErrLogger.Printf("can not send: %v", err)
				continue
			}

			if isDebug {
				h.logger.InfoLogger.Printf("user registered: %v", id)
			}
		}

		if user.IsLineInputRequired && update.Message != nil && update.Message.Text != "" {
			text := update.Message.Text
			h.logger.InfoLogger.Printf("text: %s", text)
			// @TODO use method with digit parameter from text by previous action
			continue
		}

		if update.CallbackQuery != nil {
			message := update.CallbackQuery.Message.MessageID

			for command, action := range h.services.Actions {
				if update.CallbackQuery.Data == command {
					newMarkUp, err := h.services.Vending.InlineMarkUp(action)
					if err != nil {
						h.logger.ErrLogger.Println(err)
					}

					if action.Method != "" {
						if err := h.services.Vending.CallFunc(action.Method, user); err != nil {
							h.logger.ErrLogger.Printf("can not call function %v: %v", action.Method, err)
							continue
						}
					}

					currentMarkUp := h.ChooseMarkUp(lastMarkUp, newMarkUp)
					if _, err := h.SendEditTxtAndMarkUp(id, message, action.Reply, currentMarkUp); err != nil {
						h.logger.ErrLogger.Printf("can not send: %v", err)
						continue
					}

					break
				}
			}
		}
	}

	return nil
}
