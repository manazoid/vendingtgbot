package service

import (
	"VendingTgBot/models"
	"VendingTgBot/pkg/repository"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"reflect"
)

type VendingMachine struct {
	repo  repository.Example
	items models.Items
	BotActions
}

func NewVendingService(repo repository.Example) *VendingMachine {
	return &VendingMachine{repo: repo, items: make(models.Items)}
}

func (s VendingMachine) InlineMarkUp(action models.Action) (*tgbotapi.InlineKeyboardMarkup, error) {
	var keyboard []tgbotapi.InlineKeyboardButton

	for _, option := range action.Options {
		keyboard = append(keyboard,
			tgbotapi.NewInlineKeyboardButtonData(option.Text, option.Next),
		)
	}
	output := tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(keyboard...),
	)

	return &output, nil
}

func (s VendingMachine) CallFunc(funcName string, user *models.User, v ...interface{}) error {
	val := reflect.ValueOf(s.BotActions)

	method := val.MethodByName(funcName)
	if !method.IsValid() {
		return fmt.Errorf("method not found: %s", funcName)
	}

	params := make([]reflect.Value, len(v)+2)
	params[0] = reflect.ValueOf(user)
	params[1] = reflect.ValueOf(&s.items)
	for i, arg := range v {
		params[i+2] = reflect.ValueOf(arg)
	}

	result := method.Call(params)
	if len(result) > 0 {
		if err, ok := result[0].Interface().(error); ok {
			return err
		}
	}

	return nil
}
