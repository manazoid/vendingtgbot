package service

import (
	"VendingTgBot/models"
	"VendingTgBot/pkg/repository"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type Vending interface {
	CallFunc(funcName string, user *models.User, v ...interface{}) error
	InlineMarkUp(action models.Action) (*tgbotapi.InlineKeyboardMarkup, error)
}

type Service struct {
	Vending
	Actions models.Actions
}

func NewService(repos *repository.Repository, actions models.Actions) *Service {
	return &Service{
		Vending: NewVendingService(repos.Example),
		Actions: actions,
	}
}
