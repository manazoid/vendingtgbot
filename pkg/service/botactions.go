package service

import (
	"VendingTgBot/models"
	"fmt"
)

type BotActions struct{}

func (a BotActions) CheckBalance(user *models.User, v ...interface{}) string {
	return fmt.Sprintf("текущий баланс: %d", user.Balance)
}

func (a BotActions) DepositBalance(user *models.User, v ...interface{}) string {
	amount := v[1].(int64)
	user.Balance += amount
	return a.CheckBalance(user)
}

func (a BotActions) BuyItem(user *models.User, v ...interface{}) {

}

func (a BotActions) AddItem(user *models.User, v ...interface{}) {

}
