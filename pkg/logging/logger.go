package logging

import (
	"VendingTgBot/models"
	"fmt"
	"io"
	"log"
	"log/slog"
	"os"
	"path/filepath"
	"time"
)

type Logger struct {
	InfoLogger *log.Logger
	WarnLogger *log.Logger
	ErrLogger  *log.Logger
}

func NewLogger(opt *slog.HandlerOptions, appConf *models.App) (*Logger, error) {
	currentDate := time.Now().Format("2006-01-02")

	if err := os.MkdirAll(appConf.LogFile, os.ModePerm); err != nil {
		return nil, err
	}
	fileName := fmt.Sprintf("%s%s.%s", appConf.LogPrefix, currentDate, appConf.LogExtension)

	logFileName := filepath.Join(appConf.LogFile, fileName)
	file, err := os.OpenFile(logFileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return nil, fmt.Errorf("failed to open log file: %v", err)
	}

	multiWriter := io.MultiWriter(file, os.Stdout)
	handler := slog.NewJSONHandler(multiWriter, opt)
	slog.SetDefault(slog.New(handler))

	return &Logger{
		InfoLogger: slog.NewLogLogger(handler, slog.LevelInfo),
		WarnLogger: slog.NewLogLogger(handler, slog.LevelWarn),
		ErrLogger:  slog.NewLogLogger(handler, slog.LevelError),
	}, nil
}
