package main

import (
	"VendingTgBot/models"
	"VendingTgBot/pkg/config"
	"VendingTgBot/pkg/handler"
	"VendingTgBot/pkg/logging"
	"VendingTgBot/pkg/repository"
	"VendingTgBot/pkg/service"
	"log/slog"
	"os"
)

func main() {
	configuration, logger, err := PrepareConfigLogger()
	if err != nil {
		slog.Error(err.Error())
		os.Exit(1)
	}
	isDebug := configuration.App.Mode == config.ModeDebug
	if isDebug {
		logger.WarnLogger.Println("note that debug mode is on")
	}

	connections, err := repository.NewConnection(configuration)
	if err != nil {
		logger.ErrLogger.Fatalf("can not connect to %v", err)
	}
	repos := repository.NewRepository(connections)
	services := service.NewService(repos, configuration.Actions)
	handlers := handler.NewHandler(services, logger, connections.TelegramBot)

	if err = handlers.Run(isDebug); err != nil {
		logger.ErrLogger.Printf("run failed: %v", err)
	}
}

func PrepareConfigLogger() (*models.Config, *logging.Logger, error) {
	opt := &slog.HandlerOptions{
		AddSource: true,
		Level:     slog.LevelDebug,
	}
	sLogger := slog.New(slog.NewJSONHandler(os.Stdout, opt))
	slog.SetDefault(sLogger)

	configuration, err := config.LoadConfig("config.yaml")
	if err != nil {
		return nil, nil, err
	}

	logger, err := logging.NewLogger(opt, &configuration.App)
	if err != nil {
		return nil, nil, err
	}

	return configuration, logger, nil

}
