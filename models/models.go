package models

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

type Connection struct {
	ExampleConnection *interface{}
	TelegramBot       *tgbotapi.BotAPI
}

type User struct {
	Language            string
	Balance             int64
	IsLineInputRequired bool
}

type Items map[string]Item

type Item struct {
	Quantity int64
	Price    int64
}
