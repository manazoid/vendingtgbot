package models

type Actions map[string]Action

type Action struct {
	Reply   string   `mapstructure:"reply"`
	Method  string   `mapstructure:"method"`
	Amount  int64    `mapstructure:"amount"`
	Options []Option `mapstructure:"options" validate:"nonzero"`
}

type Option struct {
	Text string `mapstructure:"text" validate:"nonzero"`
	Next string `mapstructure:"next" validate:"nonzero"`
}
