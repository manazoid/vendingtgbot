package models

type Config struct {
	App      `mapstructure:"app"`
	Telegram `mapstructure:"telegram"`
	Actions  `mapstructure:"actions" validate:"nonzero"`
}

type App struct {
	Mode         string `mapstructure:"mode" validate:"nonzero"`
	LogPrefix    string `mapstructure:"log_prefix"`
	LogFile      string `mapstructure:"log_path"`
	LogExtension string `mapstructure:"log_extension"`
}

type Telegram struct {
	ApiKey string `mapstructure:"api_key" validate:"nonzero"`
}
